This repo contains a solution to the GoPizza problem statement in the form of a console application.

Simply Run the application and give single/mulitple strings for input ( one for each pizza ) to get the total as output on the console.

please refer to test cases for sample input strings.

### Problem statement : 

GoPizza offers a variety of Pizzas to its customers with a wide set of customizations that the customers can order based on their preferences.

Below are the 5 items which they currently serve to their customers:

Veggie Supreme(Cheese, chicken, capsicum, onion, red paprika, mushroom) - Price 250
Chicken Supreme(Cheese, chicken, meatball) - Price 300
Double Chicken feast(Cheese, chicken, capsicum, onion, red paprika)- Price 350
Veg Farmhouse(Cheese, capsicum, onion, red paprika, black olives, corn) -Price 300
Margarita(Cheese)- Price 225

Toppings:

Cheese: Price 20
Olives: Price 20
Onion: Price 10
Chicken: Price 25
Cottage cheese: Price 15

Bread Types:

Regular (Default)
Thin crust: 20
Cheese crust: 25
Pan: 10

The 5 Items served could be served with a default price that is tagged against each of the Pizza mentioned, or a user can actually order it with added toppings mentioned in the toppings section. The Price would add up in the pizza as a final price. Alternatively a user can also request to remove a topping from the default menu and the price for that particular topping would then be reduced from the default menu item.


The Solution should take orders in the form of a String array something like [“Chicken Supreme, cheese, olives, Thin Crust”,“Veggie Supreme, cheese, -onion” ] and should return the total value of the order.

For storing the Menu options and the prices in memory objects like map or Enum etc could be used.


Also a user can mention the bread type the cost of which would be then calculated based on the selection.


Examples:


1. “Chicken Supreme, cheese, olives, Thin Crust”

So in this particular case

Default price for Chicken Supreme: 300

Price of added cheese/olive topping: 20

Price of Thin Crust: 20


The Price of this order would then become: 300 +20+20+20= 360


2. “Veggie Supreme, cheese, -onion”

Default price for Chicken Supreme: 250

Price of added cheese topping: 20

Price of Onion removed: 10

The Price of this order would then become: 250 + 20 - 10= 260


3. “Double Chicken feast, cheese, cottage cheese, olives, Thin Crust, -onion”

Default Price for Double Chicken feast: 350

Price of added cheese topping: 20

Price of added Cottage cheese topping: 15

Price of added olives topping: 20

Price of Thin Crust: 20

Price of Onion removed: 10

The Price of this order would then become: 350 + 20 + 15 + 20 + 20 - 10 = 415