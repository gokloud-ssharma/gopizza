﻿using System;
using System.Collections.Generic;
using Kitchen;

namespace GoPizza
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string input = "a";
                var order = new List<string>();
                while (input != String.Empty)
                {
                    input = Console.ReadLine();
                    if (input != String.Empty)
                        order.Add(input);
                }

                var pizzaOrder = order.ToEntity();

                foreach (var pizza in pizzaOrder.Pizzas)
                {
                    Console.WriteLine("Pizza : " + pizza.Name + " || Price : " + pizza.Price);
                }
                Console.WriteLine("\n\nOrder total : " + pizzaOrder.OrderTotal);
            }
            catch (NotSupportedException ex)
            {
                Console.WriteLine("Invalid data provided in request : " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("error occurred : " + ex.Message);
            }
        }
    }
}
