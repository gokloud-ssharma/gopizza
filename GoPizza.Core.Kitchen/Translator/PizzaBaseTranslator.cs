﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kitchen
{
    public static class PizzaBaseTranslator
    {
        public static PizzaBase ToPizzaBaseEntity(this List<string> ingredients)
        {
            foreach (var ingredient in ingredients)
            {
                switch (ingredient.ToLower())
                {
                    case "thin crust":
                        ingredients.Remove(ingredient);
                        return PizzaBase.GetBase(ingredient);
                    case "cheese crust":
                        ingredients.Remove(ingredient);
                        return PizzaBase.GetBase(ingredient);
                    case "pan":
                        ingredients.Remove(ingredient);
                        return PizzaBase.GetBase(ingredient);
                }
            }

            return PizzaBase.GetBase("regular");
        }
    }
}