﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public static class OrderTranslator
    {
        public static PizzaOrder ToEntity(this List<string> order)
        {
            try
            {
                var allPizzas = new List<Pizza>();
                foreach (var pizza in order)
                {
                    allPizzas.Add(pizza.ToEntity());
                }

                return new PizzaOrder()
                {
                    Pizzas = allPizzas
                };
            }
            catch (NotSupportedException ex)
            {
                throw new NotSupportedException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}
