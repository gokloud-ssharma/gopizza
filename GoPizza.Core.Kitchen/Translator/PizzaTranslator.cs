﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public static class PizzaTranslator
    {
        public static Pizza ToEntity(this string pizzaString)
        {
            try
            {
                List<string> ingredients = pizzaString.Split(',').Select(x => x.Trim()).ToList();
                var topping = new PizzaToppings();
                var pizza = new Pizza();

                // assuming sequence can be random in the pizza string
                pizza.PizzaType = ingredients.ToPizzaTypeEntity();
                pizza.PizzaBase = ingredients.ToPizzaBaseEntity();
                if (ingredients.Count > 0)
                {
                    pizza.ToppingsToBeAdded = ingredients.Where(x => !x.StartsWith('-'))
                                                   .ToList()
                                                   .ToAddToppingsEntity();

                    pizza.ToppingsToBeRemoved = ingredients.Where(x => x.StartsWith('-'))
                                                      .ToList()
                                                      .ToRemoveToppingsEntity();
                }

                SetToppings(pizza);

                return pizza;
            }
            catch (NotSupportedException ex)
            {
                throw new NotSupportedException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static void SetToppings(Pizza pizza)
        {
            pizza.Toppings = PizzaToppings.AddToppings(pizza.PizzaType.SelectedType.Key);
            pizza.Toppings.AddRange(pizza.ToppingsToBeAdded);

            foreach (var toppingToBeRemoved in pizza.ToppingsToBeRemoved)
            {
                pizza.Toppings.Remove(pizza.Toppings
                              .Where(x => x.SelectedTopping.Key == toppingToBeRemoved.SelectedTopping.Key.Substring(1))
                              .FirstOrDefault());
            }
        }
    }
}
