﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kitchen
{
    public static class ToppingsTranslator
    {
        public static List<PizzaToppings> ToAddToppingsEntity(this List<string> toppingsToBeAdded)
        {
            var addedToppings = new List<PizzaToppings>();
            foreach (var topping in toppingsToBeAdded)
            {
                switch (topping.ToLower())
                {
                    case "cheese":
                        addedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "olives":
                        addedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "onion":
                        addedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "chicken":
                        addedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "cottage cheese":
                        addedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    default:
                        throw new NotSupportedException($"Invalid ingredient : {topping}");
                }
            }
            return addedToppings;
        }

        public static List<PizzaToppings> ToRemoveToppingsEntity(this List<string> toppingsToBeAdded)
        {
            var removedToppings = new List<PizzaToppings>();
            foreach (var topping in toppingsToBeAdded)
            {
                switch (topping.ToLower())
                {
                    case "-cheese":
                        removedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "-olives":
                        removedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "-onion":
                        removedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "-chicken":
                        removedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    case "-cottage cheese":
                        removedToppings.Add(PizzaToppings.GetTopping(topping));
                        break;
                    default:
                        throw new NotSupportedException($"Invalid ingredient : {topping}");
                }
            }
            return removedToppings;
        }
    }
}