﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kitchen
{
    public static class PizzaTypeTranslator
    {
        public static PizzaType ToPizzaTypeEntity(this List<string> ingredients)
        {
            foreach (var ingredient in ingredients)
            {
                switch (ingredient.ToLower())
                {
                    case "veggie supreme":
                        ingredients.Remove(ingredient);
                        return PizzaType.GetType(ingredient);
                    case "chicken supreme":
                        ingredients.Remove(ingredient);
                        return PizzaType.GetType(ingredient);
                    case "double chicken feast":
                        ingredients.Remove(ingredient);
                        return PizzaType.GetType(ingredient);
                    case "veg farmhouse":
                        ingredients.Remove(ingredient);
                        return PizzaType.GetType(ingredient);
                    case "margarita":
                        ingredients.Remove(ingredient);
                        return PizzaType.GetType(ingredient);
                }
            }
            throw new NotSupportedException($"Unsupported pizza type provided in request");
        }
    }
}