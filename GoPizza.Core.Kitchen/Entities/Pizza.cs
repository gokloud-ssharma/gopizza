﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public class Pizza : IDish
    {
        public PizzaType PizzaType { get; set; }
        public PizzaBase PizzaBase { get; set; }
        public List<PizzaToppings> ToppingsToBeAdded { get; set; }
        public List<PizzaToppings> ToppingsToBeRemoved { get; set; }
        public List<PizzaToppings> Toppings { get; set; }
        public string Name { get { return PizzaType.SelectedType.Key; } }
        public double Price { get {
                return PizzaType.SelectedType.Value
                + PizzaBase.SelectedBase.Value
                + Toppings.Sum(x => x.SelectedTopping.Value);
                } }
    }
}
