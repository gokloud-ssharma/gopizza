﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public class PizzaToppings
    {
        public KeyValuePair<string, int> SelectedTopping = new KeyValuePair<string, int>();

        private readonly Dictionary<string, int> Toppings = new Dictionary<string, int>
        {
            { "cheese" , 20 },
            { "olives" , 20 },
            { "onion" , 10 },
            { "chicken" , 25 },
            { "cottage cheese" , 15 },
            { "-cheese" , -20 },
            { "-olives" , -20 },
            { "-onion" , -10 },
            { "-chicken" , -25 },
            { "-cottage cheese" , -15 }
        };

        private readonly Dictionary<string, List<string>> TypeToppings = new Dictionary<string, List<string>>
        {
            { "veggie supreme" , new List<string> { "cheese", "chicken", "onion" } },
            { "chicken supreme" , new List<string> { "cheese", "chicken" } },
            { "double chicken feast" , new List<string> { "cheese", "chicken", "onion" } },
            { "veg farmhouse" , new List<string> { "cheese", "onion" } },
            { "margarita" , new List<string> { "cheese" } }
        };

        public static PizzaToppings GetTopping(string ingredient)
        {
            PizzaToppings topping = new PizzaToppings();
            topping.SelectedTopping = topping.Toppings.SingleOrDefault(x => x.Key == ingredient.ToLower());

            return topping;
        }

        public static List<PizzaToppings> AddToppings(string pizzaType)
        {
            List<PizzaToppings> toppings = new List<PizzaToppings>();

            PizzaToppings topping = new PizzaToppings();
            var toppingsList = topping.TypeToppings.Where(x => x.Key == pizzaType).FirstOrDefault().Value;

            foreach(var ingredient in toppingsList)
            {
                toppings.Add(new PizzaToppings() { SelectedTopping = topping.Toppings.SingleOrDefault(x => x.Key == ingredient.ToLower()) });
            }

            return toppings;
        }
    }
}
