﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public class PizzaType
    {
        public KeyValuePair<string, int> SelectedType = new KeyValuePair<string, int>();

        private readonly Dictionary<string, int> Types = new Dictionary<string, int>
        {
            { "veggie supreme" , 195 },
            { "chicken supreme" , 255 },
            { "double chicken feast" , 295 },
            { "veg farmhouse" , 270 },
            { "margarita" , 205 }
        };

        public static PizzaType GetType(string ingredient)
        {
            PizzaType pizzaType = new PizzaType();
            pizzaType.SelectedType = pizzaType.Types.SingleOrDefault(x => x.Key == ingredient.ToLower());

            return pizzaType;
        }
    }
}
