﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public class PizzaBase
    {
        public KeyValuePair<string, int> SelectedBase = new KeyValuePair<string, int>();

        private readonly Dictionary<string, int> Bases = new Dictionary<string, int>
        {
            { "regular" , 0 },
            { "thin crust" , 20 },
            { "cheese crust" , 25 },
            { "pan" , 10 }
        };

        public static PizzaBase GetBase(string ingredient)
        {
            PizzaBase PizzaBase = new PizzaBase();
            PizzaBase.SelectedBase = PizzaBase.Bases.SingleOrDefault(x => x.Key == ingredient.ToLower());

            return PizzaBase;
        }
    }
}
