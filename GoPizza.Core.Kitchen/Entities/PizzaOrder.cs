﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kitchen
{
    public class PizzaOrder : IOrder
    {
        private Random _random = new Random();
        public List<Pizza> Pizzas { get; set; }
        public string OrderNumber { get { return _random.Next(100000000).ToString(); } set { } }
        public DateTime Timestamp { get { return DateTime.Now; } }
        public double OrderTotal { get { return Pizzas.Sum(x => x.Price); } set { } }
    }
}
