﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kitchen
{
    public interface IDish
    {
        public string Name { get; }
        public double Price { get; }
    }
}
