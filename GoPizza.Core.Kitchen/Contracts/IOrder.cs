﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kitchen
{
    interface IOrder
    {
        public string OrderNumber { get; set; }
        public DateTime Timestamp { get; }
        public double OrderTotal { get; set; }
    }
}
