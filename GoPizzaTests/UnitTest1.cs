using System;
using Xunit;
using System.Collections.Generic;
using Kitchen;

namespace GoPizzaTests
{
    public class UnitTest1
    {
        [Theory]
        [MemberData(nameof(GetData), 0, 1)]
        public void Given_OrderString_We_Should_get_order_entity(List<string> input)
        {
            var order = input.ToEntity();
            Assert.Equal("chicken supreme", order.Pizzas[0].PizzaType.SelectedType.Key.ToLower());
            Assert.Equal("thin crust", order.Pizzas[0].PizzaBase?.SelectedBase.Key.ToLower());
            Assert.Equal("cheese", order.Pizzas[0].ToppingsToBeAdded[0].SelectedTopping.Key.ToLower());
            Assert.Equal("olives", order.Pizzas[0].ToppingsToBeAdded[1].SelectedTopping.Key.ToLower());
            Assert.Equal("veggie supreme", order.Pizzas[1].PizzaType.SelectedType.Key.ToLower());
            Assert.Equal("regular", order.Pizzas[1].PizzaBase?.SelectedBase.Key.ToLower());
            Assert.Equal("cheese", order.Pizzas[1].ToppingsToBeAdded[0].SelectedTopping.Key.ToLower());
            Assert.Equal("-onion", order.Pizzas[1].ToppingsToBeRemoved[0].SelectedTopping.Key.ToLower());
            Assert.Contains(order.Pizzas[1].ToppingsToBeRemoved[0].SelectedTopping.Key.ToLower(), input[1].ToLower());
            Assert.Equal(order.Pizzas[0].Name, order.Pizzas[0].PizzaType.SelectedType.Key.ToLower());
        }

        [Theory]
        [MemberData(nameof(GetData), 0, 1)]
        public void Given_OrderString_We_Should_Get_Correct_Prices(List<string> input)
        {
            var order = input.ToEntity();
            Assert.Equal(360, order.Pizzas[0].Price);
            Assert.Equal(260, order.Pizzas[1].Price);
            Assert.Equal(415, order.Pizzas[2].Price);
            Assert.Equal(1035, order.OrderTotal);
        }

        [Theory]
        [MemberData(nameof(GetData), 1, 1)]
        public void Given_OrderString_And_Removing_Nonexisting_Topping_We_Should_Get_Correct_Prices(List<string> input)
        {
            var order = input.ToEntity();
            Assert.Equal(360, order.Pizzas[0].Price);
            Assert.Equal(260, order.Pizzas[1].Price);
            Assert.Equal(620, order.OrderTotal);
        }

        [Theory]
        [MemberData(nameof(GetData), 2, 1)]
        public void Given_OrderString_And_Adding_And_Then_Removing_Same_Topping_We_Should_Get_Correct_Prices(List<string> input)
        {
            var order = input.ToEntity();
            Assert.Equal(360, order.Pizzas[0].Price);
            Assert.Equal(260, order.Pizzas[1].Price);
            Assert.Equal(620, order.OrderTotal);
        }

        [Theory]
        [MemberData(nameof(GetData), 3, 1)]
        public void Given_OrderString_With_Invalid_Topping_Removed_Should_Throw_Exception(List<string> input)
        {
            Action act = () => input.ToEntity();
            var exception = Assert.Throws<NotSupportedException>(act);
            Assert.Equal("Invalid ingredient : -oion", exception.Message);
        }

        [Theory]
        [MemberData(nameof(GetData), 4, 1)]
        public void Given_OrderString_With_Invalid_Topping_Added_Should_Throw_Exception(List<string> input)
        {
            Action act = () => input.ToEntity();
            var exception = Assert.Throws<NotSupportedException>(act);
            Assert.Equal("Invalid ingredient : nion", exception.Message);
        }

        [Theory]
        [MemberData(nameof(GetData), 5, 1)]
        public void Given_OrderString_With_Invalid_PizzaType_Should_Throw_Exception(List<string> input)
        {
            Action act = () => input.ToEntity();
            var exception = Assert.Throws<NotSupportedException>(act);
            Assert.Equal("Unsupported pizza type provided in request", exception.Message);
        }

        [Theory]
        [MemberData(nameof(GetData), 6, 1)]
        public void Given_OrderString_With_Invalid_PizzaBase_Should_Throw_Exception(List<string> input)
        {
            Action act = () => input.ToEntity();
            var exception = Assert.Throws<NotSupportedException>(act);
            Assert.Equal("Invalid ingredient : Tin Crust", exception.Message);
        }

        public static IEnumerable<object[]> GetData(int startIndex, int numTests)
        {
            var allData = new List<object[]>
        {
            new object[] { new List<string> { "Chicken Supreme, cheese, olives, Thin Crust", "Veggie Supreme, cheese, -onion" , "Double Chicken feast, cheese, cottage cheese, olives, Thin Crust, -onion" } },
            new object[] { new List<string> { "Chicken Supreme, cheese, olives, Thin Crust, -onion", "Veggie Supreme, cheese, -onion, -cottage cheese" } },
            new object[] { new List<string> { "Chicken Supreme, cheese, olives, Thin Crust, onion, -onion", "Veggie Supreme, cheese, -onion, -cottage cheese, cottage cheese" } },
            new object[] { new List<string> { "Chicken Supreme, cheese, olives, Thin Crust, onion, -oion", "Veggie Supreme, cheese, -onion, -cottage cheee, cottage cheese" } },
            new object[] { new List<string> { "Chicken Supreme, cheese, olives, Thin Crust, nion, -onion", "Veggie Supreme, heese, -onion, -cottage cheee, cottage cheese" } },
            new object[] { new List<string> { "Chicken Sureme, cheese, olives, Thin Crust, nion, -onion", "Veggie Supreme, heese, -onion, -cottage cheee, cottage cheese" } },
            new object[] { new List<string> { "Chicken Supreme, cheese, olives, Tin Crust, onion, -onion", "Veggie Supreme, cheese, -onion, -cottage cheee, cottage cheese" } }
        };

            return allData.GetRange(startIndex, numTests);
        }
    }
}
